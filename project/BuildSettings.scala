import sbt._
import Keys._

object BuildSettings {

  val VERSION = "0.0.1-SNAPSHOT"

  lazy val basicSettings = Seq(
    version		  := VERSION,
    homepage              := Some(new URL("http://bmgconsultingltd.com")),
    organization          := "com.bmgconsulting",
    organizationHomepage  := Some(new URL("http://bmgconsultingltd.com")),
    description           := "Utilities for working with Spray",
    startYear             := Some(2014),
    scalaVersion          := "2.11.2",
    resolvers             ++= Dependencies.resolutionRepos,
    scalacOptions         := Seq(
      "-encoding", "utf8",
      "-feature",
      "-unchecked",
      "-deprecation",
      "-target:jvm-1.7",
      "-language:_",
      "-Xlog-reflective-calls"
    )
  )

}

