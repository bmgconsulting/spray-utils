import sbt._
import Keys._

object Build extends Build {

  import BuildSettings._
  import Dependencies._

  lazy val root = Project("spray-utils", file("."))
    .settings(basicSettings: _*)
    .settings(libraryDependencies ++=
      compile(sprayRouting) ++
      compile(sprayCan) ++
      compile(akkaActor) ++
      compile(akkaSlf4j) ++
      compile(spongycastle) ++
      compile(swaggerCore) ++
      compile(javaJwt) ++
      test(scalaTest) ++
      test(sprayTestkit)
    )

}

