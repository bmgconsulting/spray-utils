import sbt._

object Dependencies {

  val resolutionRepos = Seq(
    "Spray" at "http://repo.spray.io/",
    "Spray nightlies" at "http://nightlies.spray.io/",
    "Excilys Nexus" at "http://repository.excilys.com/content/groups/public",
    "Excilys Releases" at "http://repository.excilys.com/content/repositories/releases/",
    "Akka" at "http://repo.typesafe.com/typesafe/releases",
    "Fusesource" at "http://repo.fusesource.com/nexus/content/repositories/public/"
  )

  def compile(deps: ModuleID*): Seq[ModuleID] = deps map (_ % "compile" withSources())
  def provided(deps: ModuleID*): Seq[ModuleID] = deps map (_ % "provided")
  def test(deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")
  def runtime(deps: ModuleID*): Seq[ModuleID] = deps map (_ % "runtime")
  def container(deps: ModuleID*): Seq[ModuleID] = deps map (_ % "container")

  object V {

    val akka = "2.3.6"
    val Spray = "1.3.2"

  }

  val akkaActor       = "com.typesafe.akka"               %% "akka-actor"                   % V.akka
  val akkaSlf4j       = "com.typesafe.akka"               %% "akka-slf4j"                   % V.akka
  val akkaCluster     = "com.typesafe.akka"               %% "akka-cluster"                 % V.akka
  val akkaContrib     = "com.typesafe.akka"               %% "akka-contrib"                 % V.akka
  val akkaKernel      = "com.typesafe.akka"               %% "akka-kernel"                  % V.akka
  val akkaTestkit     = "com.typesafe.akka"               %% "akka-testkit"                 % V.akka
  val typesafeConfig  = "com.typesafe"                    % "config"                        % "1.2.1"

  val sprayCan        = "io.spray"                        %% "spray-can"                    % V.Spray
  val sprayRouting    = "io.spray"                        %% "spray-routing"                % V.Spray
  val sprayClient     = "io.spray"                        %% "spray-client"                 % V.Spray
  val sprayTestkit    = "io.spray"                        %% "spray-testkit"                % V.Spray

  val scalaTest       = "org.scalatest"                   %% "scalatest"                    % "2.1.3"
  val scalacheck      = "org.scalacheck"                  %% "scalacheck"                   % "1.11.3"
  val mockito         = "org.mockito"                     % "mockito-all"                   % "1.9.5"
  val sigar           = "org.fusesource"                  % "sigar"                         % "1.6.4"
  val logbackClassic  = "ch.qos.logback"                  % "logback-classic"               % "1.0.13"
  val jbcrypt         = "org.mindrot"                     % "jbcrypt"                       % "0.3m"
  val jug             = "com.fasterxml.uuid"              % "java-uuid-generator"           % "3.1.3"
  val aspectjWeaver   = "org.aspectj"                     % "aspectjweaver"                 % "1.7.3"
  val guava           = "com.google.guava"                % "guava"                         % "14.0.1"
  val bitcoinj        = "com.google"                      % "bitcoinj"                      % "0.11.2"
  val guice           = "com.google.inject"               % "guice"                         % "4.0-beta4"
  val scalaGuice      = "net.codingwell"                  %% "scala-guice"                  % "4.0.0-beta"
  val mongoJavaDriver = "org.mongodb"                     % "mongo-java-driver"             % "2.11.4"
  val mongoJack       = "org.mongojack"                   % "mongojack"                     % "2.0.0"
  val scalaTestMongo  = "com.github.simplyscala"          %% "scalatest-embedmongo"         % "0.2.1"
  val embeddedMongo   = "de.flapdoodle.embed"             % "de.flapdoodle.embed.mongo"     % "1.35"
  val jacksonCore     = "com.fasterxml.jackson.core"      % "jackson-databind"              % "2.3.3"
  val jacksonJoda     = "com.fasterxml.jackson.datatype"  % "jackson-datatype-joda"         % "2.3.3"
  val jacksonScala    = "com.fasterxml.jackson.module"    %% "jackson-module-scala"         % "2.3.3"
  val joda            = "joda-time"                       % "joda-time"                     % "2.3"
  val jodaConvert     = "org.joda"                        % "joda-convert"                  % "1.4"
  val scalamock       = "org.scalamock"                   %% "scalamock-scalatest-support"  % "3.1.RC1"
  val spongycastle    = "com.madgag"                      % "sc-light-jdk15on"              % "1.47.0.3"
  val swaggerCore     = "com.wordnik"                     %% "swagger-core"                 % "1.3.10"
  val swaggerUi       = "com.wordnik"                     %% "swagger-ui"                   % "1.3.10"
  val javaJwt         = "com.auth0"                        %  "java-jwt"                        % "1.0.0"
}

