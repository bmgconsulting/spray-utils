package com.bmgconsulting.spray

import akka.actor.{ActorRefFactory, ActorSystem, ActorContext}
import shapeless.{::, HNil}
import spray.http.HttpHeaders._
import spray.http._
import spray.routing._
import spray.util.LoggingContext


trait HttpRouter extends Directives {

  import Controller._

  protected var routes = Seq.empty[RouteFactory]

  // CORS Setup

  def corsConfig = CorsConfig.default

  protected def respondWithCORS(inner: RouteFactory)(implicit ac: ActorContext, log: LoggingContext): Route =
    respondWithCORS(inner(ac)(log))

  protected def respondWithCORS(inner: Route) = {

    respondWithHeaders(
      `Access-Control-Allow-Origin`(corsConfig.allowedOrigins),
      `Access-Control-Allow-Headers`(corsConfig.allowedHeaders),
      `Access-Control-Allow-Methods`(corsConfig.allowedMethods),
      `Access-Control-Allow-Credentials`(allow = corsConfig.allowCredentials),
      `Access-Control-Max-Age`(corsConfig.maxAge)
    ) {
      inner
    }

  }

  def route(factory: RouteFactory)(implicit ac: ActorRefFactory, log: LoggingContext): Route = factory(ac)(log)
  def route(controller: BaseResourceController)(implicit ac: ActorRefFactory, log: LoggingContext): Route = controller.route(ac)(log)

  protected def configure(route: RouteFactory): Unit = routes = routes :+ route

  def route(implicit ac: ActorRefFactory, log: LoggingContext): Route =
      routes.map(_(ac)(log)).reduce(_ ~ _)

}