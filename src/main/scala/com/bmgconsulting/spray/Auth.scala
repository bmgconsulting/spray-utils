package com.bmgconsulting.spray

import com.auth0.jwt.JWTVerifier
import spray.http.StatusCodes._
import spray.http._
import spray.routing.authentication._
import spray.routing.{Directives, Rejection, RejectionHandler, RequestContext}

import scala.concurrent.{ExecutionContext, Future}
import collection.JavaConversions._
import collection.mutable

case class Scope(values: Set[String]) {

  override def toString = values.mkString(" ")

  def |(s: Scope) = Scope(values | s.values)

  def contains(v: String) = values.contains(v)

  def contains(scope: Scope) = (scope.values -- values).isEmpty

}

object Scope {

  def apply(value: String): Scope = Scope(value.split(" ").toSet)

  val Empty = Scope(Set[String]())
  val User = Scope("user")
  val Admin = Scope("admin")

}

object OAuth extends Directives {

  sealed trait OAuthRejection extends Rejection
  case object InvalidOAuthRequest extends OAuthRejection
  case object InvalidOAuthToken extends OAuthRejection
  case object ExpiredOAuthToken extends OAuthRejection
  case object InsufficientOAuthScope extends OAuthRejection

  case class UnsupportedGrantType(grantType: String) extends OAuthRejection

  val rejectionHandler = RejectionHandler {
    case InvalidOAuthRequest :: _ => complete(BadRequest, "Invalid oAuth request")
    case InvalidOAuthToken :: _ => complete(Unauthorized, "Invalid oAuth token")
    case ExpiredOAuthToken :: _ => complete(Unauthorized, "Expired oAuth token")
    case InsufficientOAuthScope :: _ => complete(Unauthorized, "Insufficient scope")
  }

  def tokenHeader(ctx: RequestContext): Option[String] = tokenHeader(ctx.request)

  def tokenHeader(req: HttpRequest): Option[String] = {

    req.headers.find(_.name == HttpHeaders.Authorization.name)
      .map {
      case HttpHeaders.Authorization(OAuth2BearerToken(token)) => Some(token)
      case _ => None
    }.flatten
  }

}

trait JwtContextAuthenticator[Session] extends ContextAuthenticator[Session] {

  import com.bmgconsulting.spray.OAuth._

  val verifier: JWTVerifier
  implicit val ec: ExecutionContext

  def unmarshal: mutable.Map[String, AnyRef] => Session

  def apply(ctx: RequestContext): Future[Authentication[Session]] = apply(ctx.request)

  def apply(req: HttpRequest): Future[Authentication[Session]] = {

    val tokenParam = req.uri.query.get("access_token")
    val tokenOpt = tokenParam orElse tokenHeader(req)

    Future {
      tokenOpt match {
        case Some(token) => {
          val tokenMap = mapAsScalaMap(verifier.verify(token))
          Right(unmarshal(tokenMap))
        }
        case None =>
          Left(InvalidOAuthRequest)
      }  
    }

  }

}

