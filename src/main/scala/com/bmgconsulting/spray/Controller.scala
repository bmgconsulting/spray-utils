package com.bmgconsulting.spray

import akka.actor.ActorRefFactory
import com.bmgconsulting.spray.Controller.RouteFactory
import org.json4s.Formats
import org.json4s.native.Serialization
import shapeless.{::, HNil}
import spray.http._
import spray.httpx.marshalling.Marshaller
import spray.httpx.unmarshalling.Unmarshaller
import spray.routing._
import spray.routing.authentication.ContextAuthenticator
import spray.util.LoggingContext

import scala.concurrent.ExecutionContext

trait User {
  def hasScope(scope: Scope): Boolean
}

trait Session {
  def user: User
}

object Controller {

  type RouteFactory = ActorRefFactory => LoggingContext => Route

  val defaultLimit = 20
  val defaultOffset = 0

  case class Filter(limit: Option[Int], offset: Option[Int], fields: Option[List[String]]) {

    val limitWithDefault = limit.getOrElse(defaultLimit)
    val offsetWithDefault = offset.getOrElse(defaultOffset)
    val fieldsWithDefault = fields.getOrElse(Nil)

  }

}


trait Controller extends Directives {

  import com.bmgconsulting.spray.Controller._

  def authenticated(inner: Session => RouteFactory)(implicit ec: ExecutionContext, auth: ContextAuthenticator[Session]): RouteFactory = {
    ac => log => authenticate(auth) { session => inner(session)(ac)(log)}
  }

  def authorized(f: Session => Boolean)(inner: Session => RouteFactory)(implicit ec: ExecutionContext, auth: ContextAuthenticator[Session]): RouteFactory = {
    ac => log => authenticate(auth) {
      session => authorize(f(session)) {
        inner(session)(ac)(log)
      }
    }
  }

  def jsonMarshaller[A <: AnyRef](implicit formats: Formats) =
    Marshaller.of[A](ContentTypes.`application/json`) {
      (value, contentType, ctx) => {

        // TODO add selfs

        val json = Serialization.write(value)
        ctx.marshalTo(HttpEntity(contentType, json))
      }
    }

  def jsonUnmarshaller[A](implicit formats: Formats, mf: Manifest[A]) =
    Unmarshaller[A](MediaTypes.`application/json`) {
      case HttpEntity.NonEmpty(contentType, data) =>
        val json = data.asString(HttpCharsets.`UTF-8`)
        Serialization.read[A](json)
    }
}

trait BaseResourceController extends Controller {

  var routes = List.empty[RouteFactory]

  def route: RouteFactory =
    ac => log => {
      require(routes.nonEmpty, "No routes are defined")
      routes.map(_(ac)(log)).reduce(_ ~ _)
    }

}

trait ResourceController[Id] extends BaseResourceController {

  def baseMatcher: PathMatcher[HNil]

  def idMatcher: PathMatcher1[Id]

  def list(f: () => RouteFactory): Unit = {
    val route: RouteFactory = ac => log =>
      path(baseMatcher) {
        (pathEnd & get) {
          f()(ac)(log)
        }
      }
    routes = routes :+ route
  }

  def create(f: () => RouteFactory): Unit = {
    val route: RouteFactory = ac => log =>
      path(baseMatcher) {
        (pathEnd & post) {
          f()(ac)(log)
        }
      }

    routes = routes :+ route
  }

  def getById(f: (Id) => RouteFactory): Unit = {
    val route: RouteFactory = ac => log =>
      path(baseMatcher / idMatcher) { id =>
        (pathEnd & get) {
          f(id)(ac)(log)
        }
      }
    routes = routes :+ route
  }

  def update(f: (Id) => RouteFactory): Unit = {
    val route: RouteFactory = ac => log =>
      path(baseMatcher / idMatcher) { id =>
        (pathEnd & put) {
          f(id)(ac)(log)
        }
      }
    routes = routes :+ route
  }

  def destroy(f: (Id) => RouteFactory): Unit = {
    val route: RouteFactory = ac => log =>
      path(baseMatcher / idMatcher) { id =>
        (pathEnd & delete) {
          f(id)(ac)(log)
        }
      }
    routes = routes :+ route
  }

  def extra(p: String)(f: (Id) => RouteFactory): Unit = {
    val route: RouteFactory = ac => log =>
      (path(baseMatcher / idMatcher / p) & pathEnd) { id =>
        f(id)(ac)(log)
      }
    routes = routes :+ route
  }


}

trait SubResourceController[Id1, Id2] extends BaseResourceController {

  def baseMatcher: PathMatcher[::[Id1, HNil]]

  def idMatcher: PathMatcher1[Id2]

  def list(f: (Id1) => RouteFactory): Unit = {
    val route: RouteFactory = ac => log =>
      path(baseMatcher) { id1 =>
        (pathEnd & get) {
          f(id1)(ac)(log)
        }
      }
    routes = routes :+ route
  }

  def create(f: (Id1) => RouteFactory): Unit = {
    val route: RouteFactory = ac => log =>
      path(baseMatcher) { id1 =>
        (pathEnd & post) {
          f(id1)(ac)(log)
        }
      }

    routes = routes :+ route
  }

  def getById(f: (Id1, Id2) => RouteFactory): Unit = {
    val route: RouteFactory = ac => log =>
      path(baseMatcher / idMatcher) { (id1, id2) =>
        (pathEnd & get) {
          f(id1, id2)(ac)(log)
        }
      }
    routes = routes :+ route
  }

  def update(f: (Id1, Id2) => RouteFactory): Unit = {
    val route: RouteFactory = ac => log =>
      path(baseMatcher / idMatcher) { (id1, id2) =>
        (pathEnd & put) {
          f(id1, id2)(ac)(log)
        }
      }
    routes = routes :+ route
  }

  def destroy(f: (Id1, Id2) => RouteFactory): Unit = {
    val route: RouteFactory = ac => log =>
      path(baseMatcher / idMatcher) { (id1, id2) =>
        (pathEnd & delete) {
          f(id1, id2)(ac)(log)
        }
      }
    routes = routes :+ route
  }

  def extra(p: String)(f: (Id1, Id2) => RouteFactory): Unit = {
    val route: RouteFactory = ac => log =>
      (path(baseMatcher / idMatcher / p) & pathEnd) { (id1, id2) =>
        f(id1, id2)(ac)(log)
      }
    routes = routes :+ route
  }

}

trait SubSubResourceController[Id1, Id2, Id3] extends BaseResourceController {

  def baseMatcher: PathMatcher[::[Id1, ::[Id2, HNil]]]

  def idMatcher: PathMatcher1[Id3]

  def list(f: (Id1, Id2) => RouteFactory): Unit = {
    val route: RouteFactory = ac => log =>
      path(baseMatcher) { (id1, id2) =>
        (pathEnd & get) {
          f(id1, id2)(ac)(log)
        }
      }
    routes = routes :+ route
  }

  def create(f: (Id1, Id2) => RouteFactory): Unit = {
    val route: RouteFactory = ac => log =>
      path(baseMatcher) { (id1, id2) =>
        (pathEnd & post) {
          f(id1, id2)(ac)(log)
        }
      }

    routes = routes :+ route
  }

  def getById(f: (Id1, Id2, Id3) => RouteFactory): Unit = {
    val route: RouteFactory = ac => log =>
      path(baseMatcher / idMatcher) { (id1, id2, id3) =>
        (pathEnd & get) {
          f(id1, id2, id3)(ac)(log)
        }
      }
    routes = routes :+ route
  }

  def update(f: (Id1, Id2, Id3) => RouteFactory): Unit = {
    val route: RouteFactory = ac => log =>
      path(baseMatcher / idMatcher) { (id1, id2, id3) =>
        (pathEnd & put) {
          f(id1, id2, id3)(ac)(log)
        }
      }
    routes = routes :+ route
  }

  def destroy(f: (Id1, Id2, Id3) => RouteFactory): Unit = {
    val route: RouteFactory = ac => log =>
      path(baseMatcher / idMatcher) { (id1, id2, id3) =>
        (pathEnd & delete) {
          f(id1, id2, id3)(ac)(log)
        }
      }
    routes = routes :+ route
  }

  def extra(p: String)(f: (Id1, Id2, Id3) => RouteFactory): Unit = {
    val route: RouteFactory = ac => log =>
      (path(baseMatcher / idMatcher / p) & pathEnd) { (id1, id2, id3) =>
        f(id1, id2, id3)(ac)(log)
      }
    routes = routes :+ route
  }

}


