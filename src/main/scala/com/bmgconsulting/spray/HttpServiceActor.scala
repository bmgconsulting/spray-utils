package com.bmgconsulting.spray

import akka.actor.{Actor, ActorLogging}
import akka.event.{LoggingReceive, Logging}
import spray.http._
import spray.routing.HttpService
import spray.routing.directives.LogEntry


trait HttpServiceActor extends HttpService with Actor with ActorLogging {

  def router: HttpRouter

  def logEntity(prefix: String, entity: HttpEntity) = {
    entity match {
      case HttpEntity.NonEmpty(ContentType(MediaType("application/json"), _), data) => prefix + data.asString
      case HttpEntity.NonEmpty(ContentType(MediaType(mediaType), _), _) => s"${prefix}data($mediaType)"
      case _ => ""
    }
  }

  def logMessage(req: HttpRequest): Any => Option[LogEntry] = {
    case res: HttpResponse => res.status.isSuccess match {
      case true => Some(LogEntry(s"${req.method} ${req.uri.path.toString()} - ${res.message.status}", Logging.InfoLevel))
      case false => Some(LogEntry(s"${req.method} ${req.uri.path.toString()} - ${res.message.status}${logEntity("\n", req.entity)}", Logging.ErrorLevel))
    }
    case x => None
  }

  def receive: Receive = LoggingReceive {
    runRoute {
      compressResponseIfRequested() {
        decompressRequest() {
          logRequestResponse(logMessage _) {
            router.route
          }
        }
      }
    }
  }

}
