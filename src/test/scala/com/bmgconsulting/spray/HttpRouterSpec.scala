package com.bmgconsulting.spray

import akka.actor.{ActorSystem, ActorContext, ActorRefFactory}
import org.scalatest.WordSpec
import spray.http.StatusCodes
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest
import spray.http.StatusCodes._
import spray.util.LoggingContext


object HttpRouterSpec {


  // Read only controller
  class FooController extends ResourceController[Int] {

    def baseMatcher = "foo"
    def idMatcher = IntNumber

    list { () => ac => log =>
      complete(StatusCodes.OK)
    }

    getById { id => ac => log =>
      complete(id.toString)
    }

  }

  class FooBarController extends SubResourceController[Int, Int] {

    def baseMatcher = "foo" / IntNumber / "bar"
    def idMatcher = IntNumber

    create { parentId => ac => log =>
      complete(parentId.toString)
    }

    getById{ (parentId, id) => ac => log =>
      complete(id.toString)
    }

  }

  class FooBazController extends SubResourceController[Int, String] {

    def baseMatcher = "foo" / IntNumber / "baz"
    def idMatcher = Segment

    list { parentId => ac => log =>
      complete(parentId.toString)
    }

    getById { (parentId, id) => ac => log =>
      complete(id.toString)
    }

    extra("extra") { (parentId, id) => ac => log =>
      get {
        complete(id.toString)
      }
    }

  }

  trait TestHttpRouter extends HttpRouter {

    val fooController = new FooController()
    val fooBarController = new FooBarController()
    val fooBazController = new FooBazController()

    configure {
      implicit ac => log =>
          route(fooController) ~
          route(fooBarController) ~
          route(fooBazController)
    }

  }

}


class HttpRouterSpec extends WordSpec with ScalatestRouteTest with HttpRouterSpec.TestHttpRouter with HttpService {

  def actorRefFactory: ActorRefFactory = system

  "The Foo controller" should {

    "return values for READ methods" in {

      Get("/foo") ~> sealRoute(route) ~> check {
        assert(status == OK)
      }

      Get("/foo/1") ~> sealRoute(route) ~> check {
        assert(status == OK)
        assert(responseAs[String] == "1")
      }

    }

    "return MethodNotAllowed for WRITE methods" in {

      Post("/foo") ~> sealRoute(route) ~> check {
        assert(status == MethodNotAllowed)
      }

      Put("/foo/1") ~> sealRoute(route) ~> check {
        assert(status == MethodNotAllowed)
      }

      Delete("/foo") ~> sealRoute(route) ~> check {
        assert(status == MethodNotAllowed)
      }

    }

  }

  "The Bar controller" should {

    "return values for create and getById" in {

      Post("/foo/10/bar") ~> sealRoute(route) ~> check {
        assert(status == OK)
        assert(responseAs[String] == "10")
      }

      Get("/foo/10/bar/11") ~> sealRoute(route) ~> check {
        assert(status == OK)
        assert(responseAs[String] == "11")
      }

    }

    "return MethodNotAllowed for all others" in {

      Get("/foo/10/bar") ~> sealRoute(route) ~> check {
        assert(status == MethodNotAllowed)
      }

      Put("/foo/10/bar/1") ~> sealRoute(route) ~> check {
        assert(status == MethodNotAllowed)
      }

      Delete("/foo/10/bar/1") ~> sealRoute(route) ~> check {
        assert(status == MethodNotAllowed)
      }

    }

  }

  "The Baz controller" should {

    "return values for READ methods" in {

      Get("/foo/1/baz") ~> sealRoute(route) ~> check {
        assert(status == OK)
      }

      Get("/foo/1/baz/12") ~> sealRoute(route) ~> check {
        assert(status == OK)
        assert(responseAs[String] == "12")
      }

    }

    "return MethodNotAllowed for WRITE methods" in {

      Post("/foo/1/baz") ~> sealRoute(route) ~> check {
        assert(status == MethodNotAllowed)
      }

      Put("/foo/1/baz/13") ~> sealRoute(route) ~> check {
        assert(status == MethodNotAllowed)
      }

      Delete("/foo/1/baz/14") ~> sealRoute(route) ~> check {
        assert(status == MethodNotAllowed)
      }

    }

    "return values for the extra sub path" in {

      Get("/foo/1/baz/2/extra") ~> sealRoute(route) ~> check {
        assert(status == OK)
        assert(responseAs[String] == "2")
      }

    }

  }

}
